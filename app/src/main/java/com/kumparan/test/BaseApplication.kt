package com.kumparan.test

import android.app.Application
import com.kumparan.test.di.apiRepoContractModule
import com.kumparan.test.di.networkModule
import com.kumparan.test.di.prefModule
import com.kumparan.test.di.viewControllerModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class BaseApplication: Application() {

    companion object {
        val modules = listOf(
            viewControllerModule,
            apiRepoContractModule,
            networkModule,
            prefModule
        )
    }

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() = startKoin {
        androidContext(this@BaseApplication)

        val moduleList = arrayListOf<Module>().apply {
            addAll(modules)
        }

        modules(moduleList)
    }
}
