package com.kumparan.test.abstracts

import android.widget.PopupWindow
import com.kumparan.test.utils.LocaleHelper
import org.koin.android.scope.ScopeActivity

abstract class BaseActivity : ScopeActivity() {
    protected var loadingDialog: PopupWindow? = null
    protected var isAttached: Boolean = false

    override fun onResume() {
        super.onResume()
        LocaleHelper.onAttach(this@BaseActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        loadingDialog?.let {
            if (it.isShowing)
                it.dismiss()
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        isAttached = true
    }

}
