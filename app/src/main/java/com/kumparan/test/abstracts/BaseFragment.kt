package com.kumparan.test.abstracts

import org.koin.android.scope.ScopeFragment

abstract class BaseFragment: ScopeFragment() {
}
