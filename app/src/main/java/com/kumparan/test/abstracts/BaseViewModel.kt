package com.kumparan.test.abstracts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kumparan.test.utils.ViewStateModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val viewState: MutableLiveData<ViewStateModel> = MutableLiveData()
    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    protected fun Disposable.addToDisposable() = compositeDisposable.add(this)

    fun addToDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}