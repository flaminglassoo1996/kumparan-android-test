package com.kumparan.test.abstracts

import com.kumparan.test.utils.ViewStateModel

fun BaseViewModel.updateViewStateModel(viewStateList: List<ViewStateModel>) {
    if (viewStateList.any { it == ViewStateModel.LOADING }) {
        if (viewState.value != ViewStateModel.LOADING) {
            viewState.value = ViewStateModel.LOADING
        }
    }
    else if (viewStateList.any {it == ViewStateModel.FAILED}) {
        viewState.value = ViewStateModel.FAILED
    }
    else if (viewStateList.all {it == ViewStateModel.SUCCESS}) {
        viewState.value = ViewStateModel.SUCCESS
    }
}