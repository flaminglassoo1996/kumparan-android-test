package com.kumparan.test.di

import com.kumparan.test.service.ApiRepoContract
import com.kumparan.test.service.ApiRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module

val apiRepoContractModule = module {
    single<ApiRepoContract> {
        ApiRepository(
            apiServices = get(named(NETWORK_IN_APPS_NAMED))
        )
    }
}
