package com.kumparan.test.di

import com.google.gson.GsonBuilder
import com.kumparan.test.BuildConfig
import com.kumparan.test.service.ApiServices
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit.SECONDS

const val HEAD_URL = "https://jsonplaceholder.typicode.com/"

const val NETWORK_IN_APPS_NAMED = "inApps"

val networkModule = module {
    single(named(NETWORK_IN_APPS_NAMED)) {
        initRetrofit()
    }
}

fun initRetrofit(): ApiServices {
    val client =
        OkHttpClient.Builder()
            .callTimeout(30, SECONDS)
            .connectTimeout(30, SECONDS)
            .writeTimeout(30, SECONDS)
            .readTimeout(30, SECONDS)

    if (BuildConfig.BUILD_TYPE == "debug") {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.HEADERS
        val headerLogging = HttpLoggingInterceptor()
        headerLogging.level = HttpLoggingInterceptor.Level.BODY

        client.addInterceptor(headerLogging)
        client.addInterceptor(logging)
    }

    val gson = GsonBuilder().create()

    val retrofit = Retrofit.Builder()
        .baseUrl(HEAD_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(ApiServices::class.java)
}