package com.kumparan.test.di

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV
import androidx.security.crypto.EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
import androidx.security.crypto.MasterKeys
import com.kumparan.test.service.EncryptedPreferences
import com.kumparan.test.service.EncryptedPreferencesRepoContract
import com.kumparan.test.utils.LocaleHelper
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

var prefModule = module {
    single {
        provideEncryptedPreferences(androidContext())
    }
    single {
        LocaleHelper
    }
    single<EncryptedPreferencesRepoContract> {
        EncryptedPreferences(pref = get())
    }
}

const val SECURE_PREFS_FILE_KEY = "com.company.gibagi.secure_preferences"

fun provideEncryptedPreferences(context: Context): SharedPreferences {

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

        EncryptedSharedPreferences.create(
            SECURE_PREFS_FILE_KEY,
            masterKeyAlias,
            context,
            AES256_SIV,
            AES256_GCM
        )
    } else {
        context.getSharedPreferences(SECURE_PREFS_FILE_KEY, Context.MODE_PRIVATE)
    }

}
