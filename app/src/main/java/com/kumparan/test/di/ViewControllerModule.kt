package com.kumparan.test.di

import com.kumparan.test.ui.detail_photo.DetailPhotoActivity
import com.kumparan.test.ui.detail_post.CommentsAdapter
import com.kumparan.test.ui.detail_post.DetailPostActivity
import com.kumparan.test.ui.detail_post.DetailPostViewModel
import com.kumparan.test.ui.detail_user.AlbumsAdapter
import com.kumparan.test.ui.detail_user.DetailUserActivity
import com.kumparan.test.ui.detail_user.DetailUserViewModel
import com.kumparan.test.ui.post.PostActivity
import com.kumparan.test.ui.post.PostAdapter
import com.kumparan.test.ui.post.PostViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val viewControllerModule = module {

    scope(named<PostActivity>()) {
        scoped { (activity: PostActivity) ->
            PostAdapter(
                androidContext(),
                activity
            )
        }
        scoped {
            PostViewModel(
                apiRepoContract = get()
            )
        }
    }

    scope(named<DetailPostActivity>()) {
        scoped { (activity: DetailPostActivity) ->
            CommentsAdapter(
                androidContext(),
            )
        }
        scoped {
            DetailPostViewModel(
                apiRepoContract = get(),
                encryptedPreferencesRepoContract = get()
            )
        }
    }

    scope(named<DetailPhotoActivity>()) {}

    scope(named<DetailUserActivity>()) {
        scoped { (activity: DetailUserActivity) ->
            AlbumsAdapter(
                activity,
                get()
            )
        }
        scoped {
            DetailUserViewModel(
                apiRepoContract = get(),
                encryptedPreferencesRepoContract = get()
            )
        }
    }
}
