package com.kumparan.test.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PostResponse : ArrayList<PostResponseItem>()

data class PostResponseItem(
    @SerializedName("body")
    val body: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("userId")
    val userId: Int?
): Serializable