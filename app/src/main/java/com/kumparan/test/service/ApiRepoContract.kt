package com.kumparan.test.service

import com.kumparan.test.model.response.AlbumResponse
import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PhotoResponse
import com.kumparan.test.model.response.PostResponse
import com.kumparan.test.model.response.UserResponse
import io.reactivex.Single

interface ApiRepoContract {
    fun getPosts(): Single<PostResponse?>
    fun getUsers(): Single<UserResponse?>
    fun getComments(postId: Int): Single<CommentResponse?>
    fun getAlbums(userId: Int): Single<AlbumResponse?>
    fun getPhotos(): Single<PhotoResponse?>
}
