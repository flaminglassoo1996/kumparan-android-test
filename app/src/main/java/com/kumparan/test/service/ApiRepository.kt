package com.kumparan.test.service

import com.kumparan.test.model.response.AlbumResponse
import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PhotoResponse
import com.kumparan.test.model.response.PostResponse
import com.kumparan.test.model.response.UserResponse
import io.reactivex.Single

class ApiRepository(
    private val apiServices: ApiServices
) : ApiRepoContract {
    override fun getPosts(): Single<PostResponse?> = apiServices.getPosts()
    override fun getUsers(): Single<UserResponse?> = apiServices.getUsers()
    override fun getComments(postId: Int): Single<CommentResponse?> = apiServices.getComment(postId)
    override fun getAlbums(userId: Int): Single<AlbumResponse?> = apiServices.getAlbum(userId)
    override fun getPhotos(): Single<PhotoResponse?> = apiServices.getPhotos()
}
