package com.kumparan.test.service

import com.kumparan.test.model.response.AlbumResponse
import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PhotoResponse
import com.kumparan.test.model.response.PostResponse
import com.kumparan.test.model.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {
    @GET("posts")
    fun getPosts(): Single<PostResponse?>

    @GET("users")
    fun getUsers(): Single<UserResponse?>

    @GET("comments")
    fun getComment(
        @Query("postId") postId: Int?,
    ): Single<CommentResponse?>

    @GET("albums")
    fun getAlbum(
        @Query("userId") userId: Int?,
    ): Single<AlbumResponse?>

    @GET("photos")
    fun getPhotos(): Single<PhotoResponse?>
}

