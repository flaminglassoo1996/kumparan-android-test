package com.kumparan.test.service

import android.content.SharedPreferences
import com.kumparan.test.utils.ConstantManager.SARINAH_LOYALTY_SEARCH_HISTORY
import com.kumparan.test.utils.ConstantManager.SARINAH_MERCHANT_SEARCH_HISTORY
import com.kumparan.test.utils.ConstantManager.SARINAH_PROMO_SEARCH_HISTORY
import com.kumparan.test.utils.ConstantManager.SARINAH_SEARCH_HISTORY
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class EncryptedPreferences(private val pref: SharedPreferences) : EncryptedPreferencesRepoContract {

    private val editor: SharedPreferences.Editor = pref.edit()

    override fun setInt(key: String?, value: Int) {
        editor.putInt(key, value)
        editor.apply()
    }

    override fun setLong(key: String?, value: Long) {
        editor.putLong(key, value)
        editor.apply()
    }

    override fun setString(key: String?, value: String?) {
        editor.putString(key, value)
        editor.apply()
    }

    override fun setBoolean(key: String?, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    override fun remove(key: String?) {
        editor.remove(key)
        editor.apply()
    }

    override fun getString(key: String?): String? {
        return pref.getString(key, null)
    }

    override fun getBool(key: String?): Boolean {
        return pref.getBoolean(key, false)
    }

    override fun getBoolDefTrue(key: String?): Boolean {
        return pref.getBoolean(key, true)
    }

    override fun getInt(key: String?, defaultValue: Int): Int {
        return pref.getInt(key, defaultValue)
    }

    override fun getLong(key: String?, defaultValue: Int): Long {
        return pref.getLong(key, defaultValue.toLong())
    }

    override fun putHistorySearch(keyword: String) {
        putHistory(SARINAH_SEARCH_HISTORY, keyword)
    }

    override fun getHistorySearch(keyword: String): List<String>? {
        return getHistory(SARINAH_SEARCH_HISTORY, keyword)
    }

    override fun putLoyaltyHistorySearch(keyword: String) {
        putHistory(SARINAH_LOYALTY_SEARCH_HISTORY, keyword)
    }

    override fun getLoyaltyHistorySearch(keyword: String): List<String>? {
        return getHistory(SARINAH_LOYALTY_SEARCH_HISTORY, keyword)
    }

    override fun putMerchantHistorySearch(keyword: String) {
        putHistory(SARINAH_MERCHANT_SEARCH_HISTORY, keyword)
    }

    override fun getMerchantHistorySearch(keyword: String): List<String>? {
        return getHistory(SARINAH_MERCHANT_SEARCH_HISTORY, keyword)
    }

    override fun putPromoHistorySearch(keyword: String) =
        putHistory(SARINAH_PROMO_SEARCH_HISTORY, keyword)

    override fun getPromoHistorySearch(keyword: String): List<String>? =
        getHistory(SARINAH_PROMO_SEARCH_HISTORY, keyword)

    override fun clear() {
        pref.edit().clear().apply()
    }

    fun putHistory(prefKey: String, keyword: String){
        val type = object : TypeToken<MutableList<String>>() {}.type
        val gson = Gson()

        val savedKeyword: MutableList<String> = mutableListOf()
        pref.getString(prefKey, "")?.let {
            if (it.isNotEmpty()) {
                val list = gson.fromJson<MutableList<String>>(it, type)

                if (list.contains(keyword))
                    list.remove(keyword)

                savedKeyword.addAll(list)
            }

            savedKeyword.add(keyword)

            editor.putString(prefKey, gson.toJson(savedKeyword, type))
            editor.apply()
        }
    }

    fun getHistory(prefKey: String, keyword: String): List<String>?{
        val type = object : TypeToken<List<String>>() {}.type
        val gson = Gson()

        val savedKeyword = pref.getString(prefKey, "")
        savedKeyword?.let {
            if (it.isNotEmpty()) {
                val filter = gson.fromJson<List<String>>(it, type)
                val filtered: List<String> = filter.filter {
                    it.contains(keyword)
                }

                return filtered
            }
            return listOf()
        }

        return listOf()
    }
}
