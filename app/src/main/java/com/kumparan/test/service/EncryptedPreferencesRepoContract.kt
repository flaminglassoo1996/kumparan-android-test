package com.kumparan.test.service

interface EncryptedPreferencesRepoContract {
    fun setInt(key: String?, value: Int)
    fun setLong(key: String?, value: Long)
    fun setString(key: String?, value: String?)
    fun setBoolean(key: String?, value: Boolean)

    fun remove(key: String?)
    fun clear()

    fun getString(key: String?): String?
    fun getBool(key: String?): Boolean
    fun getBoolDefTrue(key: String?): Boolean
    fun getInt(key: String?, defaultValue: Int): Int
    fun getLong(key: String?, defaultValue: Int): Long

    fun putHistorySearch(keyword: String)
    fun getHistorySearch(keyword: String): List<String>?

    fun putMerchantHistorySearch(keyword: String)
    fun getMerchantHistorySearch(keyword: String): List<String>?

    fun putPromoHistorySearch(keyword: String)
    fun getPromoHistorySearch(keyword: String): List<String>?

    fun putLoyaltyHistorySearch(keyword: String)
    fun getLoyaltyHistorySearch(keyword: String): List<String>?

}
