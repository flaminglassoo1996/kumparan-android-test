package com.kumparan.test.ui.detail_photo

import android.os.Bundle
import com.kumparan.test.R
import com.kumparan.test.abstracts.BaseActivity
import com.kumparan.test.model.response.PhotoResponseItem
import com.kumparan.test.utils.CommonFunction
import kotlinx.android.synthetic.main.activity_detail_photo.activityDetailPhotoToolbar
import kotlinx.android.synthetic.main.activity_detail_photo.detailPhoto
import kotlinx.android.synthetic.main.activity_detail_photo.photoTitle

class DetailPhotoActivity : BaseActivity() {

    companion object {
        val PHOTO_DATA = "photo_data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photo)

        initLayout()

        initEvent()
    }

    private fun initEvent() {
        activityDetailPhotoToolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun initLayout() {
        val photo = intent.getSerializableExtra(PHOTO_DATA) as? PhotoResponseItem

        photoTitle.text = photo?.title ?: ""
        CommonFunction.loadImage(photo?.thumbnailUrl ?: "", detailPhoto)
    }
}