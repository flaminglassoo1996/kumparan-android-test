package com.kumparan.test.ui.detail_post

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumparan.test.R
import com.kumparan.test.model.response.CommentResponseItem
import kotlinx.android.synthetic.main.adapter_comments.view.commentAuthorName
import kotlinx.android.synthetic.main.adapter_comments.view.commentBody

class CommentsAdapter(var context: Context) : RecyclerView.Adapter<CommentsAdapter.NotificationViewHolder>() {
    var comments = mutableListOf<CommentResponseItem>()

    fun updateComments(items: List<CommentResponseItem>) {
        this.comments.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_comments, parent, false)
        return NotificationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindView(this.comments[position])
    }

    override fun getItemCount(): Int = this.comments.size

    inner class NotificationViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(data: CommentResponseItem) {
            view.commentAuthorName.text = data.name
            view.commentBody.text = data.body
        }
    }
}