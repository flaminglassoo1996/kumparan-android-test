package com.kumparan.test.ui.detail_post

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.kumparan.test.R
import com.kumparan.test.abstracts.BaseActivity
import com.kumparan.test.model.response.PostResponseItem
import com.kumparan.test.model.response.UserResponseItem
import com.kumparan.test.ui.detail_user.DetailUserActivity
import kotlinx.android.synthetic.main.activity_detail_post.activityDetailPostToolbar
import kotlinx.android.synthetic.main.activity_detail_post.comments
import kotlinx.android.synthetic.main.activity_detail_post.commentsLoading
import kotlinx.android.synthetic.main.activity_detail_post.commentsReload
import kotlinx.android.synthetic.main.activity_detail_post.layoutAdapterPost
import kotlinx.android.synthetic.main.activity_detail_post.recyclerComments
import kotlinx.android.synthetic.main.adapter_post.view.postBody
import kotlinx.android.synthetic.main.adapter_post.view.postTitle
import kotlinx.android.synthetic.main.adapter_post.view.userCompany
import kotlinx.android.synthetic.main.adapter_post.view.username
import org.koin.core.parameter.parametersOf

class DetailPostActivity : BaseActivity() {

    companion object {
        val POST_DATA = "post_data"
        val USER_DATA = "user_data"
    }

    private val detailPostViewModel: DetailPostViewModel by inject {
        parametersOf(this)
    }
    private val commentsAdapter: CommentsAdapter by inject {
        parametersOf(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_post)

        initLayout()

        initObserver()

        initEvent()

        getData()
    }

    private fun initEvent() {
        commentsReload.setOnClickListener {
            getData()
        }

        activityDetailPostToolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        layoutAdapterPost.username.setOnClickListener {
            val user = intent.getSerializableExtra(USER_DATA) as? UserResponseItem

            val intent = Intent(this, DetailUserActivity::class.java)
            intent.putExtra(DetailUserActivity.USER_DATA, user)
            startActivity(intent)
        }
    }

    private fun getData() {
        val post = intent.getSerializableExtra(POST_DATA) as? PostResponseItem

        detailPostViewModel.getComments(post?.id ?: 0)
    }

    private fun initObserver() {
        detailPostViewModel.commentsLoading.observe(this) {
            recyclerComments.visibility = if (it) View.INVISIBLE else View.VISIBLE
            commentsLoading.visibility = if (it) View.VISIBLE else View.GONE
        }

        detailPostViewModel.commentsError.observe(this) {
            commentsReload.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }

        detailPostViewModel.comments.observe(this) {
            if (!it.isEmpty()) {
                commentsAdapter.updateComments(it)
                comments.text = "${it.size} ${if (it.size == 1) "comment" else "comments"}"
            }
        }
    }

    private fun initLayout(){
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recyclerComments.apply {
            layoutManager = linearLayoutManager
            adapter = commentsAdapter
        }

        val user = intent.getSerializableExtra(USER_DATA) as? UserResponseItem
        val post = intent.getSerializableExtra(POST_DATA) as? PostResponseItem

        user?.let { _user ->
            layoutAdapterPost.username.text = _user.name
            layoutAdapterPost.userCompany.text = _user.company?.name ?: ""
            layoutAdapterPost.postTitle.text = post?.title ?: ""
            layoutAdapterPost.postBody.text = post?.body ?: ""
        }
    }
}