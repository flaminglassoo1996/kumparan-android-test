package com.kumparan.test.ui.detail_post

import androidx.lifecycle.MutableLiveData
import com.kumparan.test.abstracts.BaseViewModel
import com.kumparan.test.model.response.CommentResponseItem
import com.kumparan.test.service.ApiRepoContract
import com.kumparan.test.service.EncryptedPreferencesRepoContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailPostViewModel (
    private val apiRepoContract: ApiRepoContract, private val encryptedPreferencesRepoContract: EncryptedPreferencesRepoContract
        ) : BaseViewModel() {

    var comments: MutableLiveData<ArrayList<CommentResponseItem>> = MutableLiveData()
    var commentsLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var commentsError: MutableLiveData<Boolean> = MutableLiveData(false)

    fun getComments(postId: Int) {
        commentsLoading.value = true

        apiRepoContract.getComments(postId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    commentsLoading.value = false
                    commentsError.value = it == null

                    it?.let { _comments ->
                        comments.value = _comments
                    }
                },
                {
                    commentsError.value = true
                    commentsLoading.value = false
                }
            ).addToDisposable()
    }
}