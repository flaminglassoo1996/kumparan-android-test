package com.kumparan.test.ui.detail_user

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kumparan.test.R
import com.kumparan.test.model.response.AlbumResponseItem
import com.kumparan.test.model.response.PhotoResponseItem
import com.kumparan.test.ui.detail_photo.DetailPhotoActivity
import kotlinx.android.synthetic.main.adapter_album.view.albumName
import kotlinx.android.synthetic.main.adapter_album.view.emptyAlbum
import kotlinx.android.synthetic.main.adapter_album.view.photosLoading
import kotlinx.android.synthetic.main.adapter_album.view.recyclerPhoto

class AlbumsAdapter(var activity: DetailUserActivity, var detailUserViewModel: DetailUserViewModel) : RecyclerView.Adapter<AlbumsAdapter.NotificationViewHolder>(), PhotosAdapterListener {

    var albums = mutableListOf<AlbumResponseItem>()

    val photosAdapter = PhotosAdapter(this)

    fun updateAlbums(items: List<AlbumResponseItem>) {
        this.albums.addAll(items)
        notifyDataSetChanged()
    }

    fun updatePhotos(photos: List<PhotoResponseItem>, index: Int) {
        photosAdapter.updatePhotos(photos)
        notifyItemChanged(index)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_album, parent, false)
        return NotificationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindView(this.albums[position])
    }

    override fun getItemCount(): Int = this.albums.size

    inner class NotificationViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(data: AlbumResponseItem) {
            view.albumName.text = data.title

            detailUserViewModel.photosLoading.observe(activity) {
                view.photosLoading.visibility = if (it) View.VISIBLE else View.GONE
                if (!it) view.emptyAlbum.visibility = if (photosAdapter.itemCount == 0) View.VISIBLE else View.GONE
            }

            val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

            view.recyclerPhoto.apply {
                layoutManager = linearLayoutManager
                adapter = photosAdapter
            }
        }
    }

    override fun onPhotoClick(item: PhotoResponseItem) {
        val intent = Intent(activity, DetailPhotoActivity::class.java)
        intent.putExtra(DetailPhotoActivity.PHOTO_DATA, item)
        activity.startActivity(intent)
    }
}