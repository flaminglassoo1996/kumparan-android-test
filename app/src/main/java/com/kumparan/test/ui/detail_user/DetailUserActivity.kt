package com.kumparan.test.ui.detail_user

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.kumparan.test.R
import com.kumparan.test.abstracts.BaseActivity
import com.kumparan.test.model.response.UserResponseItem
import kotlinx.android.synthetic.main.activity_detail_user.activityDetailUserToolbar
import kotlinx.android.synthetic.main.activity_detail_user.albumsLoading
import kotlinx.android.synthetic.main.activity_detail_user.albumsReload
import kotlinx.android.synthetic.main.activity_detail_user.recyclerAlbums
import kotlinx.android.synthetic.main.activity_detail_user.userAddress
import kotlinx.android.synthetic.main.activity_detail_user.userCompany
import kotlinx.android.synthetic.main.activity_detail_user.userEmail
import kotlinx.android.synthetic.main.activity_detail_user.username
import org.koin.core.parameter.parametersOf

class DetailUserActivity : BaseActivity() {

    companion object {
        val USER_DATA = "user_data"
    }

    private val detailUserViewModel: DetailUserViewModel by inject {
        parametersOf(this)
    }
    private val albumsAdapter: AlbumsAdapter by inject {
        parametersOf(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_user)

        initLayout()

        initObserver()

        initEvent()

        getData()
    }

    private fun initEvent() {
        albumsReload.setOnClickListener { getData() }

        activityDetailUserToolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun getData() {
        val user = intent.getSerializableExtra(USER_DATA) as? UserResponseItem

        user?.let { _user ->
            username.text = _user.name
            userCompany.text = _user.company?.name ?: ""
            userAddress.text = user.address?.street ?: ""
            userEmail.text = user.email ?: ""
        }

        detailUserViewModel.getAlbums(user?.id ?: 0)
        detailUserViewModel.getPhotos()
    }

    private fun initObserver() {
        detailUserViewModel.albumsLoading.observe(this) {
            recyclerAlbums.visibility = if (it) View.INVISIBLE else View.VISIBLE
            albumsLoading.visibility = if (it) View.VISIBLE else View.GONE
        }

        detailUserViewModel.albumsError.observe(this) {
            albumsReload.visibility = if (it) View.VISIBLE else View.GONE
        }

        detailUserViewModel.albums.observe(this) {
            if (!it.isEmpty()) {
                albumsAdapter.updateAlbums(it)
            }
        }

        detailUserViewModel.photos.observe(this) {
            if (!it.isEmpty() && !(detailUserViewModel.albums.value ?: mutableListOf()).isEmpty()) {
                detailUserViewModel.albums.value?.forEachIndexed { index, _albums ->
                    albumsAdapter.updatePhotos(it.filter { _photos -> _photos.albumId == _albums.id }, index)
                }
            }
        }
    }

    private fun initLayout(){
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recyclerAlbums.apply {
            layoutManager = linearLayoutManager
            adapter = albumsAdapter
        }
    }
}