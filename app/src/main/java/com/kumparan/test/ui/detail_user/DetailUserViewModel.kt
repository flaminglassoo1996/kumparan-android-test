package com.kumparan.test.ui.detail_user

import androidx.lifecycle.MutableLiveData
import com.kumparan.test.abstracts.BaseViewModel
import com.kumparan.test.model.response.AlbumResponseItem
import com.kumparan.test.model.response.PhotoResponseItem
import com.kumparan.test.service.ApiRepoContract
import com.kumparan.test.service.EncryptedPreferencesRepoContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailUserViewModel (
    private val apiRepoContract: ApiRepoContract, private val encryptedPreferencesRepoContract: EncryptedPreferencesRepoContract
        ) : BaseViewModel() {

    var photos: MutableLiveData<ArrayList<PhotoResponseItem>> = MutableLiveData()
    var albums: MutableLiveData<ArrayList<AlbumResponseItem>> = MutableLiveData()
    var albumsLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var albumsError: MutableLiveData<Boolean> = MutableLiveData(false)
    var photosLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var photosError: MutableLiveData<Boolean> = MutableLiveData(false)

    fun getAlbums(userId: Int) {
        albumsLoading.value = true

        apiRepoContract.getAlbums(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    albumsLoading.value = false
                    albumsError.value = it == null

                    it?.let { _albums ->
                        albums.value = _albums
                    }
                },
                {
                    albumsError.value = true
                    albumsLoading.value = false
                }
            ).addToDisposable()
    }

    fun getPhotos() {
        photosLoading.value = true

        apiRepoContract.getPhotos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    photosLoading.value = false
                    photosError.value = it == null

                    it?.let { _photos ->
                        photos.value = _photos
                    }
                },
                {
                    photosError.value = true
                    photosLoading.value = false
                }
            ).addToDisposable()
    }
}