package com.kumparan.test.ui.detail_user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumparan.test.R
import com.kumparan.test.model.response.PhotoResponseItem
import com.kumparan.test.utils.CommonFunction
import kotlinx.android.synthetic.main.adapter_photos.view.photoImage

interface PhotosAdapterListener {
    fun onPhotoClick(item: PhotoResponseItem)
}

class PhotosAdapter(var listener: PhotosAdapterListener) : RecyclerView.Adapter<PhotosAdapter.NotificationViewHolder>() {
    var photos = mutableListOf<PhotoResponseItem>()

    fun updatePhotos(items: List<PhotoResponseItem>) {
        this.photos.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_photos, parent, false)
        return NotificationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindView(this.photos[position])
    }

    override fun getItemCount(): Int = this.photos.size

    inner class NotificationViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(data: PhotoResponseItem) {
            CommonFunction.loadImage(data.thumbnailUrl, view.photoImage)

            view.photoImage.setOnClickListener {
                listener.onPhotoClick(data)
            }
        }
    }
}