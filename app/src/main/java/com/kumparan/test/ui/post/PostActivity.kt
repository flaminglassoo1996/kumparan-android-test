package com.kumparan.test.ui.post

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.kumparan.test.R
import com.kumparan.test.abstracts.BaseActivity
import com.kumparan.test.model.response.PostResponseItem
import com.kumparan.test.model.response.UserResponseItem
import com.kumparan.test.ui.detail_post.DetailPostActivity
import com.kumparan.test.ui.detail_user.DetailUserActivity
import kotlinx.android.synthetic.main.activity_post.postsLoading
import kotlinx.android.synthetic.main.activity_post.postsReload
import kotlinx.android.synthetic.main.activity_post.recyclerPostsList
import org.koin.core.parameter.parametersOf

class PostActivity : BaseActivity(), PostAdapterListener {
    private val postViewModel: PostViewModel by inject {
        parametersOf(this)
    }
    private val postAdapter: PostAdapter by inject {
        parametersOf(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        initLayout()

        initObserver()

        initEvent()

        getData()
    }

    private fun initEvent() {
        postsReload.setOnClickListener {
            getData()
        }
    }

    private fun getData() {
        postViewModel.getPosts()
        postViewModel.getUsers()
    }

    private fun initObserver() {
        postViewModel.postsLoading.observe(this) {
            recyclerPostsList.visibility = if (it) View.INVISIBLE else View.VISIBLE
            postsLoading.visibility = if (it) View.VISIBLE else View.GONE
        }

        postViewModel.postsError.observe(this) {
            postsReload.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }

        postViewModel.posts.observe(this) {
            if (!it.isEmpty()) postAdapter.updatePosts(it)
        }

        postViewModel.users.observe(this) {
            if (!it.isEmpty()) postAdapter.updateUsers(it)
        }
    }

    private fun initLayout(){
        val linearLayoutManager = LinearLayoutManager(this@PostActivity, LinearLayoutManager.VERTICAL, false)

        recyclerPostsList.apply {
            layoutManager = linearLayoutManager
            adapter = postAdapter
        }
    }

    override fun onPostClick(data: PostResponseItem, user: UserResponseItem?) {
        val intent = Intent(this, DetailPostActivity::class.java)
        intent.putExtra(DetailPostActivity.POST_DATA, data)
        intent.putExtra(DetailPostActivity.USER_DATA, user)
        startActivity(intent)
    }

    override fun onUsernameClick(data: UserResponseItem?) {
        val intent = Intent(this, DetailUserActivity::class.java)
        intent.putExtra(DetailUserActivity.USER_DATA, data)
        startActivity(intent)
    }
}