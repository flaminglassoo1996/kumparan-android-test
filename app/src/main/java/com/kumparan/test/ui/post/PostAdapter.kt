package com.kumparan.test.ui.post

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kumparan.test.R
import com.kumparan.test.model.response.PostResponseItem
import com.kumparan.test.model.response.UserResponseItem
import kotlinx.android.synthetic.main.adapter_post.view.containerPostItem
import kotlinx.android.synthetic.main.adapter_post.view.postBody
import kotlinx.android.synthetic.main.adapter_post.view.postTitle
import kotlinx.android.synthetic.main.adapter_post.view.userCompany
import kotlinx.android.synthetic.main.adapter_post.view.username

interface PostAdapterListener {
    fun onPostClick(data: PostResponseItem, user: UserResponseItem?)
    fun onUsernameClick(data: UserResponseItem?)
}

class PostAdapter(var context: Context, var listener: PostAdapterListener) : RecyclerView.Adapter<PostAdapter.NotificationViewHolder>() {
    var posts = mutableListOf<PostResponseItem>()
    var users = mutableListOf<UserResponseItem>()

    fun updatePosts(items: List<PostResponseItem>) {
        this.posts.addAll(items)
        notifyDataSetChanged()
    }

    fun updateUsers(items: List<UserResponseItem>) {
        this.users.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_post, parent, false)
        return NotificationViewHolder(view)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        holder.bindView(
            this.posts[position],
            this.users.singleOrNull { _user -> _user.id == this.posts[position].userId })
    }

    override fun getItemCount(): Int = this.posts.size

    inner class NotificationViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(data: PostResponseItem, user: UserResponseItem?) {
            view.postTitle.text = data.title
            view.postBody.text = data.body
            view.username.text = if (user == null) "Loading..." else user.name ?: "-"
            view.userCompany.text = if (user == null) "Loading..." else user.company?.name ?: "-"

            view.containerPostItem.setOnClickListener {
                listener.onPostClick(data, user)
            }

            view.username.setOnClickListener {
                listener.onUsernameClick(user)
            }
        }
    }
}