package com.kumparan.test.ui.post

import androidx.lifecycle.MutableLiveData
import com.kumparan.test.abstracts.BaseViewModel
import com.kumparan.test.model.response.PostResponseItem
import com.kumparan.test.model.response.UserResponseItem
import com.kumparan.test.service.ApiRepoContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostViewModel(
    private val apiRepoContract: ApiRepoContract
) : BaseViewModel() {

    var posts: MutableLiveData<ArrayList<PostResponseItem>> = MutableLiveData()
    var users: MutableLiveData<ArrayList<UserResponseItem>> = MutableLiveData()
    var postsLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var postsError: MutableLiveData<Boolean> = MutableLiveData(false)
    var usersError: MutableLiveData<Boolean> = MutableLiveData(false)

    fun getPosts() {
        postsLoading.value = true

        apiRepoContract.getPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    postsLoading.value = false
                    postsError.value = it == null

                    it?.let { _posts ->
                        posts.value = _posts
                    }
                },
                {
                    postsError.value = true
                    postsLoading.value = false
                }
            ).addToDisposable()
    }

    fun getUsers() {
        apiRepoContract.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    usersError.value = it == null

                    it?.let { _users ->
                        users.value = _users
                    }
                },
                {
                    usersError.value = true
                }
            ).addToDisposable()
    }
}