package com.kumparan.test.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.io.File
import java.text.SimpleDateFormat

val dateFormatShownDOB = SimpleDateFormat("dd MMM yyyy")
val dateFormatDobParam = SimpleDateFormat("yyyy-MM-dd")

class CommonFunction {
    companion object {
        fun loadImage(url: String?, view: ImageView) {
            if (url != null && url.isNotEmpty())
                Picasso.get().load(url).into(view)
        }

        fun copyText(context: Context, text: String?) {
            if (text != null) {
                val clipboard = context.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                val clip: ClipData = ClipData.newPlainText("sarinah", text)
                clipboard.setPrimaryClip(clip)
            }
        }

        fun getFileFromUri(context: Context, uri: Uri?): File? {
            if (uri == null || uri.path == null) {
                return null
            }
            var realPath = String()
            val databaseUri: Uri
            val selection: String?
            val selectionArgs: Array<String>?
            if (uri.path!!.contains("/document/image:")) {
                databaseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                selection = "_id=?"
                selectionArgs = arrayOf(DocumentsContract.getDocumentId(uri).split(":")[1])
            } else {
                databaseUri = uri
                selection = null
                selectionArgs = null
            }
            try {
                val column = "_data"
                val projection = arrayOf(column)
                val cursor = context.contentResolver.query(
                    databaseUri,
                    projection,
                    selection,
                    selectionArgs,
                    null
                )
                cursor?.let {
                    if (it.moveToFirst()) {
                        val columnIndex = cursor.getColumnIndexOrThrow(column)
                        realPath = cursor.getString(columnIndex)
                    }
                    cursor.close()
                }
            } catch (e: Exception) {
            }
            val path = if (realPath.isNotEmpty()) realPath else {
                when {
                    uri.path!!.contains("/document/raw:") -> uri.path!!.replace(
                        "/document/raw:",
                        ""
                    )
                    uri.path!!.contains("/document/primary:") -> uri.path!!.replace(
                        "/document/primary:",
                        "/storage/emulated/0/"
                    )
                    else -> return null
                }
            }
            return File(path)
        }
    }
}