package com.kumparan.test.utils

object ConstantManager {
    // * PLAY STORE
    const val PLAY_STORE_URL = "https://.www.google.com"

    // * ARGS
    const val PHONE_NO_ARGS = "phone_no_args"
    const val COUNTRY_CODE_ARGS = "country_code_args"
    const val IS_OTP_ARGS = "is_otp_args"
    const val VERIFY_TOKEN_ARGS = "verify_token_args"
    const val USER_STATUS_ARGS = "user_status_args"
    const val IS_CHANGE_PHONE_NO = "is_change_phone_no"
    const val IS_RESET_PASS_ARGS = "is_reset_pass_args"
    const val IS_REGIST_OR_ACTIVATE = "is_regist_or_activate"

    // * LOGIN
    const val IS_LOGIN = "is_login"
    const val IS_LOGIN_AS_GUEST = "is_login_as_guest"
    const val USER_DATA = "user_data"

    // * about sarinah
    const val STORE_MAPS_ARGS = "store_maps_args"
    const val STORE_ID_ARGS = "store_id_args"
    const val STORES_ARGS = "stores_args"

    // * FCM
    const val CAN_RECEIVE_FCM = "can_receive_fcm"
    const val FCM_TOKEN = "fcm_token"

    // * Onboarding
    const val IS_FINISH_ONBOARDING = "is_finish_onboarding"

    const val RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DATE_FORMAT = "dd MMMM yyyy"

    const val SARINAH_NUMBER = "081211007270"

    const val SARINAH_SEARCH_HISTORY = "sarinah_search_history"
    const val SARINAH_MERCHANT_SEARCH_HISTORY = "sarinah_merchant_search_history"
    const val SARINAH_PROMO_SEARCH_HISTORY = "sarinah_promo_search_history"
    const val SARINAH_LOYALTY_SEARCH_HISTORY = "sarinah_loyalty_search_history"

    // * Merchant
    const val MERCHANT_ID = "merchantId"

    // * Wayfinding
    const val FLOOR_PLAN_ID = "floorId"
    const val LOCATION_ID = "locationId"
    const val IS_AREA = "isArea"
    const val SCALE_FLOOR = 3
}