package com.kumparan.test.utils

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.text.Html
import android.text.Spanned
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale
import android.graphics.BitmapFactory
import android.util.Base64


const val FULL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val SIMPLE_DATE_FORMAT = "dd MMMM yyyy"
const val SHORT_MONTH_FORMAT = "dd MMM yyyy"
const val SHORT_DATE_FORMAT = "yyyy-MM-dd"
const val SHORT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

class ConvertUtil {
    companion object {
        fun dpToPx(context: Context, dp: Int): Int {
            return (dp * context.resources.displayMetrics.density).toInt()
        }

        fun toCurrency(value: Int?): String? {
            if (value == null) return "-"
            val separator = DecimalFormatSymbols()
            separator.decimalSeparator = ','
            separator.groupingSeparator = '.'
            val formatter = DecimalFormat("#,###,###", separator)
            return formatter.format(value)
        }

        fun dateStringToString(dateString: String?, newFormat: String = SIMPLE_DATE_FORMAT, oldFormat: String = FULL_DATE_TIME_FORMAT) : String {
            var result = "-"
            if (dateString != null)
                try {
                    val format = SimpleDateFormat(oldFormat, Locale.getDefault())
                    val date = format.parse(dateString)
                    format.applyPattern(newFormat)
                    result = if (date != null) format.format(date) else result
                } catch (e: ParseException) { }

            return result
        }

        fun dateStringToCalendar(dateString: String, format: String = FULL_DATE_TIME_FORMAT) : Calendar? {
            val formatter = SimpleDateFormat(format, Locale.getDefault())
            val date = formatter.parse(dateString)
            if (date != null) {
                val calendar = GregorianCalendar()
                calendar.time = date
                return calendar
            }
            return null
        }

        fun dateStringToStringRange(startDateString: String, endDateString: String, format: String = FULL_DATE_TIME_FORMAT, longMonth: Boolean = true) : String {
            val start = dateStringToCalendar(startDateString, format)
            val end = dateStringToCalendar(endDateString, format)

            if (startDateString == endDateString) return dateStringToString(startDateString, SIMPLE_DATE_FORMAT, format)
            if (start != null && end != null) {
                val style = if (longMonth) Calendar.LONG else Calendar.SHORT
                val locale = Locale.getDefault()
                val startYear = start.get(Calendar.YEAR)
                val endYear = end.get(Calendar.YEAR)
                val startMonth = start.get(Calendar.MONTH)
                val endMonth = end.get(Calendar.MONTH)
                val startDate = start.get(Calendar.DATE)
                val endDate = end.get(Calendar.DATE)

                val isSameYear = startYear == endYear
                val isSameMonth = startMonth == endMonth
                var month = ""
                var year = ""
                if (!isSameYear)
                    year = " $startYear"
                if (!isSameYear || !isSameMonth)
                    month = " ${start.getDisplayName(Calendar.MONTH, style, locale)}"
                return "$startDate$month$year - $endDate ${end.getDisplayName(Calendar.MONTH, style, locale)} $endYear"
            }
            return "-"
        }

        fun stringToDate(dateString: String, pattern: String): Date? {
            val format = SimpleDateFormat(pattern, Locale.getDefault())
            return format.parse(dateString)
        }

        fun stringToString(dateString: String, pattern: String = FULL_DATE_TIME_FORMAT, newPattern: String = SIMPLE_DATE_FORMAT): String? {
            val oldDate = SimpleDateFormat(pattern, Locale.getDefault()).parse(dateString) ?: return "-"

            return SimpleDateFormat(newPattern, Locale.getDefault()).format(oldDate)
        }

        fun getCurrentDate(pattern: String): String {
            val format = SimpleDateFormat(pattern, Locale.getDefault())
            return format.format(Date())
        }

        fun fromHtml(html: String?): Spanned? {
            if (html != null) {
                return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    Html.fromHtml(html)
                }
            }
            return null
        }

        fun base64Image(data: String) : Bitmap {
            val bytes: ByteArray = Base64.decode(data, Base64.DEFAULT)
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        }
    }
}