package com.kumparan.test.utils

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridDividerItemDecoration (
    private val mHorizontalDivider: Drawable,
    private val mVerticalDivider: Drawable,
    private val mNumColumns: Int
) :
    RecyclerView.ItemDecoration() {


    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.childCount > 0) {
            drawHorizontalDividers(c, parent)
            drawVerticalDividers(c, parent)
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {

        super.getItemOffsets(outRect, view, parent, state)
        val childIsRightMost = parent.getChildAdapterPosition(view) % mNumColumns - 1 == 0
        if (!childIsRightMost)
            outRect.right = mHorizontalDivider.intrinsicWidth
        if (childIsRightMost)
            outRect.left = mHorizontalDivider.intrinsicWidth
        val childIsInFirstRow = parent.getChildAdapterPosition(view) < mNumColumns
        if (!childIsInFirstRow) {
            outRect.top = mVerticalDivider.intrinsicHeight
        }
    }

    private fun drawHorizontalDividers(canvas: Canvas, parent: RecyclerView) {
        val lastRowChildIndex: Int = parent.childCount - 1
        val columns = maxOf(mNumColumns, parent.childCount)
        for (i in 0 until columns - 1) {
            val firstRowChild: View = parent.getChildAt(i)
            val lastRowChild: View = parent.getChildAt(lastRowChildIndex)
            val dividerTop: Int = firstRowChild.top
            val dividerLeft = firstRowChild.right + mHorizontalDivider.intrinsicWidth/2
            val dividerRight: Int = dividerLeft + mHorizontalDivider.intrinsicWidth
            val dividerBottom: Int = lastRowChild.bottom
            mHorizontalDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
            mHorizontalDivider.draw(canvas)
        }
    }
    private fun drawVerticalDividers(canvas: Canvas, parent: RecyclerView) {
        val childCount = parent.childCount
        val rowCount = childCount / mNumColumns
        val rightmostChildIndex: Int = if (rowCount != 0) {
            rowCount * mNumColumns - 1
        } else 0
        for (i in 1..rowCount) {
            val leftmostChild: View = parent.getChildAt(i * mNumColumns - mNumColumns)
            val rightmostChild: View = parent.getChildAt(rightmostChildIndex)
            val dividerLeft: Int = leftmostChild.left
            val dividerBottom: Int = leftmostChild.bottom
            val dividerTop = dividerBottom - mVerticalDivider.intrinsicHeight
            val dividerRight: Int = rightmostChild.right
            mVerticalDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
            mVerticalDivider.draw(canvas)
        }
    }
}