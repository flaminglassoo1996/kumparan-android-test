package com.kumparan.test.utils

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridSpacingItemDecorationHorizontal(context: Context, private val spanCount: Int, space: Int) : RecyclerView.ItemDecoration() {
    private val spaceInDp = ConvertUtil.dpToPx(context, space)
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column

        outRect.top = column * spaceInDp / spanCount // column * ((1f / spanCount) * spacing)
        outRect.bottom =
            spaceInDp - (column + 1) * spaceInDp / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
        if (position >= spanCount) {
            outRect.left = spaceInDp // item top
        }
    }
}