package com.kumparan.test.utils

import android.text.TextUtils
import android.util.Patterns
import java.util.regex.Pattern

object InputValidator {
    fun isValidEmail(target: String): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    private val p: Pattern = Pattern.compile("^[a-zA-Z0-9]*$")

    fun isAlphaNumeric(s: String?): Boolean {
        return p.matcher(s).find()
    }

    fun removeZeroPrefix(s: String?): String? =
        s?.replaceFirst(Regex("^0+"), "")

    private val passwordPattern: Pattern = Pattern.compile("(?i:.*(?:[A-Z].*[0-9]|[0-9].*[A-Z]).*)")

    fun isValidPassword(s: String?): Boolean{
        return passwordPattern.matcher(s).find()
    }
}
