package com.kumparan.test.utils

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import androidx.preference.PreferenceManager
import java.util.Locale

const val SELECTED_LANGUAGE = "Locale.Helper.Selected.Language"

object LocaleHelper {

    fun onAttach(context: Context): Context? {
        val lang = getPersistedData(context)
        return setLocale(context, lang?.toLowerCase())
    }

    fun getLanguage(context: Context): String? {
        return getPersistedData(context)
    }

    internal fun setLocale(context: Context, language: String?): Context? {
        persist(context, language)

        //23
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
            return updateResourcesLegacy(context, language)
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
            return updateResourcesLegacy(context, language)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return updateResourcesLegacy(context, language)
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N || Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
            return updateResourcesLegacy(context, language)
        }
        return updateResources(context, language)
    }

    private fun getPersistedData(context: Context): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(SELECTED_LANGUAGE, "in")
    }

    private fun persist(context: Context, language: String?) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString(SELECTED_LANGUAGE, language)
        editor.apply()
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResources(context: Context, language: String?): Context? {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration = Configuration()
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

    private fun updateResourcesLegacy(context: Context, language: String?): Context {
        val resources: Resources = context.resources
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration = Configuration()
        configuration.setLocale(locale)
        //unknown reason, cannot use new function createConfigurationContext
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            context.createConfigurationContext(configuration)
//        } else {
            resources.updateConfiguration(configuration, resources.displayMetrics)
//        }
        return context
    }

}
