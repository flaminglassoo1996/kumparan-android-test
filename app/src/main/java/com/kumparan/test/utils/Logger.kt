package com.kumparan.test.utils

import android.util.Log
import com.kumparan.test.BuildConfig

object Logger {
    fun debugLog(tag: String? = "LoggerDefaultTag", message: String) {
        if (BuildConfig.BUILD_TYPE == "debug") {
            Log.d(tag, message)
        }
    }
}
