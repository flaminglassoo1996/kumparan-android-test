package com.kumparan.test.utils

enum class ViewStateModel {
    LOADING,
    FAILED,
    SUCCESS
}
