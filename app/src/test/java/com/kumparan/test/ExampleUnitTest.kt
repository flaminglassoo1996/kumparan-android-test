package com.kumparan.test

import com.google.gson.GsonBuilder
import com.kumparan.test.di.HEAD_URL
import com.kumparan.test.model.response.PostResponseItem
import com.kumparan.test.test_class.MockApiRepoContract
import com.kumparan.test.test_class.MockApiRepository
import com.kumparan.test.test_class.MockApiServices
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Test
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

const val MOCK_NETWORK = "mock_network"

internal fun mockInitRetrofit(): MockApiServices {
    val client =
        OkHttpClient.Builder()
            .callTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

    val gson = GsonBuilder().create()

    val retrofit = Retrofit.Builder()
        .baseUrl(HEAD_URL)
        .client(client.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    return retrofit.create(MockApiServices::class.java)
}

val apiModule = module {
    single(named(MOCK_NETWORK)) {
        mockInitRetrofit()
    }
}

val networkModule = module {
    single<MockApiRepoContract> {
        MockApiRepository(
            apiServices = get(named(MOCK_NETWORK))
        )
    }
}

@KoinApiExtension
class ExampleUnitTest: KoinComponent {

    @Test
    fun httpRequest_checkRequestPostThenDetailPostComment() {
        startKoin {
            val moduleList = arrayListOf<Module>().apply {
                addAll(listOf(apiModule, networkModule))
            }

            modules(moduleList)
        }

        val apiRepoContract: MockApiRepoContract = get()

        try {
            val response = apiRepoContract.getPosts().execute()

            response.body()?.let { _postResponse ->
                if (_postResponse.isEmpty()) return@let

                val expectedFirstPostTitle = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"

                val firstPost = _postResponse.first()

                /// Check first post title
                Assert.assertEquals(expectedFirstPostTitle, firstPost.title)

                println("FirstPostTitle => ${firstPost.title}")

                /// Check total comment of first post
                checkTotalCommentOfSpecificPost(apiRepoContract, firstPost)
            }
        } catch (e: IOException) { }
    }

    private fun checkTotalCommentOfSpecificPost(mockApiRepoContract: MockApiRepoContract, post: PostResponseItem) {
        try {
            val response = mockApiRepoContract.getComments(post.id ?: 0).execute()

            response.body()?.let { _commentResponse ->
                if (_commentResponse.isEmpty()) return@let

                val expectedTotalComment = 5

                println("TotalCommentPost => ${_commentResponse.size}")

                Assert.assertEquals(expectedTotalComment, _commentResponse.size)
            }
        } catch (e: IOException) { }
    }
}
