package com.kumparan.test.test_class

import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PostResponse
import retrofit2.Call

interface MockApiRepoContract {
    fun getPosts(): Call<PostResponse?>
//    fun getUsers(): Single<UserResponse?>
    fun getComments(postId: Int): Call<CommentResponse?>
//    fun getAlbums(userId: Int): Single<AlbumResponse?>
//    fun getPhotos(): Single<PhotoResponse?>
}
