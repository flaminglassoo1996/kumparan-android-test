package com.kumparan.test.test_class

import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PostResponse
import retrofit2.Call

class MockApiRepository(
    var apiServices: MockApiServices
): MockApiRepoContract {
    override fun getPosts(): Call<PostResponse?> = apiServices.getPosts()
    override fun getComments(postId: Int): Call<CommentResponse?> = apiServices.getComment(postId)

}