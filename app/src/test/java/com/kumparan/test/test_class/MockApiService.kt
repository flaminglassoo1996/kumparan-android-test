package com.kumparan.test.test_class

import com.kumparan.test.model.response.AlbumResponse
import com.kumparan.test.model.response.CommentResponse
import com.kumparan.test.model.response.PhotoResponse
import com.kumparan.test.model.response.PostResponse
import com.kumparan.test.model.response.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MockApiServices {
    @GET("posts")
    fun getPosts(): Call<PostResponse?>

    @GET("users")
    fun getUsers(): UserResponse?

    @GET("comments")
    fun getComment(
        @Query("postId") postId: Int?,
    ): Call<CommentResponse?>

    @GET("albums")
    fun getAlbum(
        @Query("userId") userId: Int?,
    ): AlbumResponse?

    @GET("photos")
    fun getPhotos(): PhotoResponse?
}